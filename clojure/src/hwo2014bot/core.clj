(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "EXCEPTION: " e))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(def track (atom []))
(def throttle-vals (atom '()))

(defmulti handle-msg :msgType)

(defmethod handle-msg "gameInit" [msg]
  (reset! track (vec (flatten 
                  (repeat 2 
                          (get-in msg 
                                  [:data :race :track :pieces])))))
  (handle-msg {}))

(defn weighted-angle [piece position-data]
  (let [angle (or (:angle piece) 0)]
    (if (and (zero? (get-in position-data [:lane :startLaneIndex]))
             (neg? angle))
      (* angle 1.1)
      angle)))

(defn sum-angles [from cnt position-data]
  (reduce + (map #(Math/abs (weighted-angle % position-data)) 
                  (take cnt 
                        (subvec @track 
                          (if (neg? from) (+ (count @track) from) from))))))

(defn me [msg]
  (first (filter #(= "Dr. Who" (get-in % [:id :name])) (:data msg))))

(defmethod handle-msg "carPositions" [msg]  
  {:msgType "throttle" 
   :data (let [position-data (:piecePosition (me msg))
               now (:pieceIndex position-data)
               slip (Math/abs (:angle (me msg)))
               throttle (- 0.6
                           (/ slip 300)
                           (/ (sum-angles (- now 4) 7 position-data) 
                              900))
               final-thr-val (if (neg? throttle) 0 throttle)]
            
           (if (= now 0)
             (println (:lap position-data)))
           (swap! throttle-vals conj final-thr-val)
           final-thr-val)})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameInit" (println (get-in msg [:data :race :track :pieces])) 
    "gameStart" (println "Race started")
    "crash" (println "CRASH!!!\n" (clojure.string/join "\n" (take 20 @throttle-vals)))
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
